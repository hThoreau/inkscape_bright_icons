# inkscape_bright_icons
> A set of bright UI icons, suited for a dark themed Inkscape.

This collection is an assortment of icons from various authors (that I won't bother to list, but only to warn that I'm not its original author) completed with some icons missing from inkscape version 0.91 to 0.92 adapted from their original design.

I'm putting this out there because, as of the time of this writting, I haven't found any bright icon set that sufficiently covers the variety of icons present in the 0.92 release of Inkscape. So there you have it.

## Install
```
cd ~/.config/inkscape/icons
wget https://gitlab.com/hThoreau/inkscape_bright_icons/raw/master/icons.svg
inkscape
```

*Voilá!*

